$(document).ready(function() {
	if (typeof addthis != "undefined") {
		addThis();
	}
});

function addThis(){
    addthis.layers({
                'theme' : 'transparent',
                'share' : {
                  'position' : 'left',
                  'services' : 'facebook,twitter,google_plusone_share,pinterest',
                  'offset' : {'top' : '230px'}
                },
                'thankyou' : 'false'
            });
}