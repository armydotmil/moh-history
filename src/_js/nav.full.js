$(document).ready(function() {
	$("a[@href^='http']").attr("target", "_blank");
	lastBlock = $("#a1");
	maxWidth = 295;
	minWidth = 84;
	
	lastBlock.animate({
		width: maxWidth + "px"
	}, {
		queue: false,
		duration: 450
	});
	
	$("ul li a").hover(function() {
		$(lastBlock).animate({
			width: minWidth + "px"
		}, {
			queue: false,
			duration: 450
		});
		$(this).animate({
			width: maxWidth + "px"
		}, {
			queue: false,
			duration: 450
		});
		lastBlock = this
	}, function() {});
	$("#bottomNav").serialScroll({
		target: "#images",
		items: "li",
		prev: "a.prev",
		next: "a.next",
		axis: "xy",
		queue: false,
		event: "click",
		stop: false,
		lock: true,
		duration: 600,
		start: 0,
		force: true,
		cycle: false,
		step: 1,
		jump: false,
		lazy: false,
		interval: false,
		constant: true,
		onBefore: function(e, t, n, r, i) {
			e.preventDefault();
			if (this.blur) this.blur()
		},
		onAfter: function(e) {}
	})
});
